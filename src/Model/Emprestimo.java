package Model;


public class Emprestimo {
	//Atributos	
	private int idEmprestimo;
	private String dataEmprestimo;
	private String dataDevolucao;
	private Usuario leitor;
	private Livro livro;
	
	//Getters e Setters
	
	public int getIdEmprestimo() {
		return idEmprestimo;
	}
	public void setIdEmprestimo(int idEmprestimo) {
		this.idEmprestimo = idEmprestimo;
	}
	public String getDataEmprestimo() {
		return dataEmprestimo;
	}
	public void setDataEmprestimo(String dataEmprestimo) {
		this.dataEmprestimo = dataEmprestimo;
	}
	public String getDataDevolucao() {
		return dataDevolucao;
	}
	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
	public Usuario getLeitor() {
		return leitor;
	}
	public void setLeitor(Usuario leitor) {
		this.leitor = leitor;
	}
	public Livro getLivro() {
		return livro;
	}
	public void setLivro(Livro livro) {
		this.livro = livro;
	}
	
	

}
