package Model;

import java.util.ArrayList;

public class Livro {
	
// Atributos
	
	private String autor;
	private String editora;
	private int idLivro;
	private String titulo;
	private String ano;
	private String idioma;
	private ArrayList<Livro> listaLivros;
	
//Getters e Setters
	
	public ArrayList<Livro> getListaLivros() {
		return listaLivros;
	}
	public void setListaLivros(ArrayList<Livro> listaLivros) {
		this.listaLivros = listaLivros;
	}

	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public int getIdLivro() {
		return idLivro;
	}
	public void setIdLivro(int idLivro) {
		this.idLivro = idLivro;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
	//Métodos

		public String adicionar(Livro umLivro) {
			String mensagem = "Livro adicionado com Sucesso!";
			listaLivros.add(umLivro);
			return mensagem;
			
		}

		public String excluir(Livro umLivro) {
			String mensagem = "Livro excluído com Sucesso!";
			listaLivros.remove(umLivro);
			return mensagem;
			
		}

		public Livro pesquisarTitulo (String umTitulo) {
		    for (Livro umLivro: listaLivros) {
		        if (umLivro.getTitulo().equalsIgnoreCase(umTitulo)) return umLivro;
		    }
		    return null;
		}
		
public String alterarAutor (String nomePesquisa, String umAutor){
			
			Livro pesquisar = pesquisarTitulo(nomePesquisa);
			Livro primitivo;
			primitivo = pesquisar;
			excluir(primitivo);
			pesquisar.setAutor(umAutor);
			adicionar(pesquisar);
			String mensagem = "Autor alterado com sucesso!";
			return mensagem;
			
			}

public String alterarEditora (String nomePesquisa, String umaEditora){
	
	Livro pesquisar = pesquisarTitulo(nomePesquisa);
	Livro primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setEditora(umaEditora);
	adicionar(pesquisar);
	String mensagem = "Editora alterada com sucesso!";
	return mensagem;
	
	}
public String alterarTitulo (String nomePesquisa, String umTitulo){
	
	Livro pesquisar = pesquisarTitulo(nomePesquisa);
	Livro primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setTitulo(umTitulo);
	adicionar(pesquisar);
	String mensagem = "Título alterado com sucesso!";
	return mensagem;
	
	}

public String alterarAno (String nomePesquisa, String umAno){
	
	Livro pesquisar = pesquisarTitulo(nomePesquisa);
	Livro primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setAno(umAno);
	adicionar(pesquisar);
	String mensagem = "Ano alterado com sucesso!";
	return mensagem;
	
	}

public String alterarIdioma (String nomePesquisa, String umIdioma){
	
	Livro pesquisar = pesquisarTitulo(nomePesquisa);
	Livro primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setIdioma(umIdioma);
	adicionar(pesquisar);
	String mensagem = "Idioma alterado com sucesso!";
	return mensagem;
	
	}


}
