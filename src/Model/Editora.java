package Model;

import java.util.ArrayList;

public class Editora {
	//Atributos
	private int idEditora;
	private String nomeEditora;
	private ArrayList<Editora> listaEditoras;
	//Getters e Setters
	public ArrayList<Editora> getListaEDitoras() {
		return listaEditoras;
	}
	public void setListaEDitoras(ArrayList<Editora> listaEDitoras) {
		this.listaEditoras = listaEDitoras;
	}
	public int getIdEditora() {
		return idEditora;
	}
	public void setIdEditora(int idEditora) {
		this.idEditora = idEditora;
	}
	public String getNomeEditora() {
		return nomeEditora;
	}
	public void setNomeEditora(String nomeEditora) {
		this.nomeEditora = nomeEditora;
	}
	
	//Métodos

		public String adicionar(Editora umaEditora) {
			String mensagem = "Editora adicionada com Sucesso!";
			listaEditoras.add(umaEditora);
			return mensagem;
			
		}

		public String excluir(Editora umaEditora) {
			String mensagem = "Aluno excluído com Sucesso!";
			listaEditoras.remove(umaEditora);
			return mensagem;
			
		}

		public Editora pesquisarPorNome (String umNome) {
		    for (Editora umaEditora: listaEditoras) {
		        if (umaEditora.getNomeEditora().equalsIgnoreCase(umNome)) return umaEditora;
		    }
		    return null;
		
		    	
		}
		
		public String alterarNome (String nomePesquisa, String umNome){
			
			Editora pesquisar = pesquisarPorNome(nomePesquisa);
			Editora primitivo;
			primitivo = pesquisar;
			excluir(primitivo);
			pesquisar.setNomeEditora(umNome);
			adicionar(pesquisar);
			String mensagem = "Editora alterada com sucesso!";
			return mensagem;
			
			
		}
}

