package Model;

import java.util.ArrayList;

public class Autor {
	//Atributos
	private int idAutor;
	private String nomeAutor;
	private ArrayList<Autor> listaAutores;
	
	//Getters e Setters	
	public ArrayList<Autor> getListaAutores() {
		return listaAutores;
	}
	public void setListaAutores(ArrayList<Autor> listaAutores) {
		this.listaAutores = listaAutores;
	}
	public int getIdAutor() {
		return idAutor;
	}
	public void setIdAutor(int idAutor) {
		this.idAutor = idAutor;
	}
	public String getNomeAutor() {
		return nomeAutor;
	}
	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}
	
	//Métodos

		public String adicionar(Autor umAutor) {
			String mensagem = "Autor adicionado com Sucesso!";
			listaAutores.add(umAutor);
			return mensagem;
			
		}

		public String excluir(Autor umAutor) {
			String mensagem = "Autor excluído com Sucesso!";
			listaAutores.remove(umAutor);
			return mensagem;
			
		}

		public Autor pesquisarPorNome (String umNome) {
		    for (Autor umAutor: listaAutores) {
		        if (umAutor.getNomeAutor().equalsIgnoreCase(umNome)) return umAutor;
		    }
		    return null;
		}

	
		public String alterarNome (String nomePesquisa, String umNome){
			
			Autor pesquisar = pesquisarPorNome(nomePesquisa);
			Autor primitivo;
			primitivo = pesquisar;
			excluir(primitivo);
			pesquisar.setNomeAutor(umNome);
			adicionar(pesquisar);
			String mensagem = "Autor alterado com sucesso!";
			return mensagem;
			
			
		}
	
}
