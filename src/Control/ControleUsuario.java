package Control;

import java.util.ArrayList;

import Model.Aluno;
import Model.Bibliotecario;




public class ControleUsuario {
	private ArrayList<Aluno> listaAlunos;
	
	//Getters e Setters	
	public ArrayList<Aluno> getListaAlunos() {
		return listaAlunos;
	}

	public void setListaAlunos(ArrayList<Aluno> listaAlunos) {
		this.listaAlunos = listaAlunos;
	}
	//Métodos Aluno
	public String adicionar(Aluno umAluno) {
		String mensagem = "Aluno adicionado com Sucesso!";
		listaAlunos.add(umAluno);
		return mensagem;
		
	}

	public String excluir(Aluno umAluno) {
		String mensagem = "Aluno excluído com Sucesso!";
		listaAlunos.remove(umAluno);
		return mensagem;
		
	}

	public Aluno pesquisarPorNomeAluno (String umNome) {
	    for (Aluno umAluno: listaAlunos) {
	        if (umAluno.getNome().equalsIgnoreCase(umNome)) return umAluno;
	    }
	    return null;
	}
	
	public String alterarNome (String nomePesquisa, String umNome){
		
		Aluno pesquisar = pesquisarPorNomeAluno(nomePesquisa);
		Aluno primitivo;
		primitivo = pesquisar;
		excluir(primitivo);
		pesquisar.setNome(umNome);
		adicionar(pesquisar);
		String mensagem = "Aluno alterado com sucesso!";
		return mensagem;
		
		
	}
	public String alterarMatricula (String nomePesquisa, String umaMatricula){
		
		Aluno pesquisar = pesquisarPorNomeAluno(nomePesquisa);
		Aluno primitivo;
		primitivo = pesquisar;
		excluir(primitivo);
		pesquisar.setMatricula(umaMatricula);
		adicionar(pesquisar);
		String mensagem = "Aluno alterado com sucesso!";
		return mensagem;
		
	}
	
public String alterarTelefone (String nomePesquisa, String umTelefone){
		
		Aluno pesquisar = pesquisarPorNomeAluno(nomePesquisa);
		Aluno primitivo;
		primitivo = pesquisar;
		excluir(primitivo);
		pesquisar.setTelefone(umTelefone);
		adicionar(pesquisar);
		String mensagem = "Aluno alterado com sucesso!";
		return mensagem;
		
	}
public String alterarEmail (String nomePesquisa, String umEmail){
	
	Aluno pesquisar = pesquisarPorNomeAluno(nomePesquisa);
	Aluno primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setEmail(umEmail);
	adicionar(pesquisar);
	String mensagem = "Aluno alterado com sucesso!";
	return mensagem;
	
}

public String alterarEndereco (String nomePesquisa, String umEndereco){
	
	Aluno pesquisar = pesquisarPorNomeAluno(nomePesquisa);
	Aluno primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setEndereco(umEndereco);
	adicionar(pesquisar);
	String mensagem = "Aluno alterado com sucesso!";
	return mensagem;

	
}



	
	private ArrayList<Bibliotecario> listaBibliotecarios;
	
	
	//Getters e Setters
	public ArrayList<Bibliotecario> getListaBibliotecarios() {
		return listaBibliotecarios;
	}

	public void setListaBibliotecarios(ArrayList<Bibliotecario> listaBibliotecarios) {
		this.listaBibliotecarios = listaBibliotecarios;
	}

	//Métodos Bibliotecario	
	public String adicionar(Bibliotecario umBibliotecario) {
		String mensagem = "Bibliotecário adicionado com Sucesso!";
		listaBibliotecarios.add(umBibliotecario);
		return mensagem;
		
	}

	public String excluir(Bibliotecario umBibliotecario) {
		String mensagem = "Bibliotecário excluído com Sucesso!";
		listaBibliotecarios.remove(umBibliotecario);
		return mensagem;
		
	}

	public Bibliotecario pesquisarPorNomeBibliotecario (String umNome) {
	    for (Bibliotecario umBibliotecario: listaBibliotecarios) {
	        if (umBibliotecario.getNome().equalsIgnoreCase(umNome)) return umBibliotecario;
	    }
	    return null;
	}
	
public String alterarNomeBibl (String nomePesquisa, String umNome){
		
		Bibliotecario pesquisar = pesquisarPorNomeBibliotecario(nomePesquisa);
		Bibliotecario primitivo;
		primitivo = pesquisar;
		excluir(primitivo);
		pesquisar.setNome(umNome);
		adicionar(pesquisar);
		String mensagem = "Bibliotecátio alterado com sucesso!";
		return mensagem;
		
		
	}
	
	
public String alterarTelefoneBibl (String nomePesquisa, String umTelefone){
		
		Bibliotecario pesquisar = pesquisarPorNomeBibliotecario(nomePesquisa);
		Bibliotecario primitivo;
		primitivo = pesquisar;
		excluir(primitivo);
		pesquisar.setTelefone(umTelefone);
		adicionar(pesquisar);
		String mensagem = "Bibliotecário alterado com sucesso!";
		return mensagem;
		
	}
public String alterarEmailBibl (String nomePesquisa, String umEmail){
	
	Bibliotecario pesquisar = pesquisarPorNomeBibliotecario(nomePesquisa);
	Bibliotecario primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setEmail(umEmail);
	adicionar(pesquisar);
	String mensagem = "Bibliotecário alterado com sucesso!";
	return mensagem;
	
}

public String alterarEnderecoBibl (String nomePesquisa, String umEndereco){
	
	Bibliotecario pesquisar = pesquisarPorNomeBibliotecario(nomePesquisa);
	Bibliotecario primitivo;
	primitivo = pesquisar;
	excluir(primitivo);
	pesquisar.setEndereco(umEndereco);
	adicionar(pesquisar);
	String mensagem = "Bibliotecário alterado com sucesso!";
	return mensagem;

	
}

}
