package Control;



	import java.util.ArrayList;

import Model.Emprestimo;
	public class ControleEmprestimo {	
		
		//Métodos

		private ArrayList<Emprestimo> listaEmprestimos;
		//Getters e Setters
		public ArrayList<Emprestimo> getListaEmprestimos() {
			return listaEmprestimos;
		}
		public void setListaEmprestimos(ArrayList<Emprestimo> listaEmprestimos) {
			this.listaEmprestimos = listaEmprestimos;
		}

			public String realizar(Emprestimo umEmprestimo) {
				String mensagem = "Empréstimo realizado com Sucesso!";
				listaEmprestimos.add(umEmprestimo);
				return mensagem;
				
			}

			public String finalizar(Emprestimo umEmprestimo) {
				String mensagem = "Emprestimo finalizado com Sucesso!";
				listaEmprestimos.remove(umEmprestimo);
				return mensagem;
				
			}
			
			public Emprestimo pesquisarPorId (int umId) {
			    for (Emprestimo umEmprestimo: listaEmprestimos) {
			        if (umEmprestimo.getIdEmprestimo() == umId) return umEmprestimo;
			    }
			    return null;
			}
			
			public String renovarEmprestimo (int idPesquisa, String umaData){
				
				Emprestimo pesquisar = pesquisarPorId(idPesquisa);
				
				Emprestimo primitivo;
				primitivo = pesquisar;
				finalizar(primitivo);
				pesquisar.setDataDevolucao(umaData);
				realizar(pesquisar);
				String mensagem = "Empréstimo renovado com sucesso!";
				return mensagem;
				
				
			}
			
	
}
